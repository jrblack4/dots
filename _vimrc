set nocompatible
filetype plugin on

" ColorScheme
colorscheme onedark
syntax on


call plug#begin('~/.vim/plugged')
" Git "
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

" Core "
Plug 'szw/vim-tags'
Plug 'adelarsq/vim-matchit'
Plug 'vim-scripts/SyntaxAttr.vim'

" IDE "
Plug 'joshdick/onedark.vim'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'MattesGroeger/vim-bookmarks'
Plug 'bling/vim-bufferline'
Plug 'tpope/vim-surround'
Plug 'editorconfig/editorconfig-vim'
Plug 'tsiemens/vim-aftercolors'
Plug 'qpkorr/vim-bufkill'
Plug 'Raimondi/delimitMate'
Plug 'voldikss/vim-floaterm'

" Ruby "
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-rails'
Plug 'keith/rspec.vim'
Plug 'thoughtbot/vim-rspec'
Plug 'tpope/vim-rake'
Plug 'victorfeijo/binding-pry-vim'
Plug 'tpope/vim-endwise'

" Test "
Plug 'vim-test/vim-test'
Plug 'tpope/vim-dispatch'
call plug#end()

filetype plugin on
filetype indent on


" Settings "
let loaded_matchparen=1
let mapleader = ","

set backspace=indent,eol,start
set clipboard+=unamed,unamedplus,autoselect
set encoding=UTF-8
set fillchars+=vert:\
set list
set listchars=tab:⇥\ ,trail:·
set mouse=a
set nofixendofline
set nostartofline
set number
set shiftwidth=2
set shortmess+=I
set softtabstop=0 noexpandtab
set splitright splitbelow
set tabstop=2

" Window Navigation "
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap H ^
nnoremap L $

" Show file name in tmux tab "
autocmd BufEnter * let &titlestring = '  ' . expand("%:t")
set title
let &titleold = '  ' . expand("%:t")

" Disable Arrow Keys in Escape Mode "
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Disable Arrow Keys in Insert Mode "
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

" Get text syntax name for setting colors "
map ;x :call SyntaxAttr() <CR>

" Generic Serach "
map ;e :e **/

" NERDTree "
nnoremap <leader>1 :NERDTreeToggle<CR>
let g:nerdtree_tabs_open_on_gui_startup=0
let g:NERDTreeMinimalUI=1
let g:NERDTreeDirArrows=1
let g:NERDTreeHighlightCursorLine=0

" vim-test "
nmap <silent> <leader>s :TestNearest<CR>
nmap <silent> <leader>t :TestFile<CR>
" nmap <silent> <leader>a :TestSuite<CR>
" nmap <silent> <leader>l :TestLast<CR>
" nmap <silent> <leader>g :TestVisit<CR>

" Rubocop
map ;r :! rubocop --format simple % <CR>
map; ;a :! rubocop -a % <CR>

" Python/RobotFramework Config/shortcuts
map ;p :! python3 % <CR>
nmap <silent> <leader>r :!robot --listener RobotStackTracer %<CR>

let g:test#preserve_screen = 0 " 1 to enable "
let g:test#echo_command = 0 " 1 to enable "

" Make test commands execute using dispatch.vim
" vimterminal
" kitty
" floaterm
let test#strategy = "vimterminal"
