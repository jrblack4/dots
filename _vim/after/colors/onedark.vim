highlight Normal ctermbg=NONE guibg=NONE
highlight LineNr ctermbg=NONE guibg=NONE
highlight CursorLineNr ctermbg=42 guibg=NONE
highlight CursorLine ctermbg=246
highlight VertSplit ctermbg=239

highlight MatchParen ctermbg=220 ctermfg=221

highlight Pmenu ctermfg=38 ctermbg=236
highlight PmenuSel ctermfg=green ctermbg=239
highlight Comment ctermfg=240

" Git Gutter Color Fixes
highlight SignColumn ctermbg=NONE
highlight GitGutterAdd ctermbg=42 ctermfg=green
highlight GitGutterChange ctermbg=94 ctermfg=yellow
highlight GitGutterDelete ctermbg=52 ctermfg=red
highlight GitGutterChangeDelete ctermbg=52 ctermfg=red

" Ruby Colors "
highlight rubyInstanceVariable ctermfg=140
highlight rubyBlockParameter ctermfg=38
highlight rubyLocalvariableorMethod ctermfg=38
highlight rubyConstant ctermfg=215
highlight rubyInterpolationDelimiter ctermfg=203
highlight rubySymbol ctermfg=221

" Test/RSpec "
highlight	rubyTestMacro ctermfg=39
highlight	rspecGroupMethods ctermfg=170

" ERB
highlight erubyDelimiter ctermfg=145

" JavaScript
highlight	jsArrowFunction ctermfg=180
highlight jsFunCall ctermfg=75
highlight	jsString ctermfg=114
highlight jsFuncParens ctermfg=180
highlight jsFuncBraces ctermfg=165
highlight jsParens ctermfg=75
highlight jsObjectProp ctermfg=75
highlight jsParen ctermfg=220
highlight jsFuncArgs ctermfg=220

" Vim Colors
highlight StatusLine ctermbg=42 ctermfg=239
highlight StatusLineNC ctermbg=239 ctermfg=146
highlight Visual term=reverse ctermbg=38 ctermfg=189
highlight vimCommand ctermfg=170
highlight type ctermfg=blue
highlight Number ctermfg=215
highlight vimVar ctermfg=38
highlight vimOption ctermfg=215
highlight vimNotation ctermfg=38
highlight vimMapModKey ctermfg=220
highlight vimString ctermfg=215
highlight vimUsrCmd ctermfg=170

highlight Special ctermfg=170

" VimWiki
highlight VimwikiHeader1 ctermfg=170
highlight VimwikiHeader2 ctermfg=220

" HTML
highlight htmlTagName ctermfg=38
highlight htmlArg ctermfg=38

" NerdTree
highlight NERDTreeDir ctermfg=74
highlight NERDTreeDirSlash ctermfg=NONE

" Robot/Python
highlight robotTable ctermfg=114
highlight builtInLibrary ctermfg=140
highlight robotTestCaseName ctermfg=221
